const { NodeMediaServer } = require('node-media-server')

const config = {
  rtmp: {
  	port: 1935,
      	chunk_size: 60000,
      	gop_cache: true,
      	ping: 60,
      	ping_timeout: 30,
      	mediaroot: './media/'
  },
  http: {
  	port: 8000,
        allow_origin: '*',
  },
  trans: {
  	ffmpeg: './ffmpeg/ffmpeg',
  	tasks: [
  		{
  			app: 'live',
  			hls: true,
  			hlsFlags: '[hls_time=2:hls_list_size=3:hls_flags=delete_segments]',
  		}
  	]
  }
}
                                    
let nms = new NodeMediaServer(config)
nms.run()
