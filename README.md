# Node Media Server + HLS Muxing com FFmpeg

Nesta implementação o servidor já faz o mux para o HLS que é o formato ideal para a parte VOD.

## Instalação

1. Instalar o NodeJS
2. Correr no terminal sudo npm install
3. Build Estática do FFmpeg - https://www.johnvansickle.com/ffmpeg/
3. Ficheiros Media - https://drive.google.com/drive/folders/1qDAbQHuRl_6ehbnl76p6j_TxsQdBWmHi

### Execução

No terminal:  
1. sudo node index.js  
2. abrir outro separador e executar o ffmpeg (ver ficheiro "comandos")  
3. abrir a stream num cliente por ex. VLC e pôr http://[ip]:8000/live/test  

## Autor

* **Bruno Moreira**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
